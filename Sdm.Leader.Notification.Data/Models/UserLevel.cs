﻿namespace Sdm.Leader.Notification.Data.Models
{
    public class UserLevel
    {
        public int Id { get; set; }

        public string? Description { get; set; }

        public List<NotificationRecipient> NotificationRecipients { get; set; } = new();

        public List<User> Users { get; set; } = new();
    }
}
