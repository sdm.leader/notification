﻿namespace Sdm.Leader.Notification.Data.Models
{
    public class UserQueueNotification
    {
        public long Id { get; set; }

        public long UserId { get; set; }

        public long NotificationId { get; set; }

        public Notification Notification { get; set; } = new();

        public User User { get; set; } = new();
    }
}
