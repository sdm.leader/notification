﻿using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Notification.Data.Models
{
    public class NotificationRecipient
    {
        public long Id { get; set; }

        public UserRole? UserRole { get; set; }

        public int? UserLevelId { get; set; }

        public long? UserId { get; set; }

        public User? User { get; set; }

        public UserLevel? UserLevel { get; set; }
    }
}
