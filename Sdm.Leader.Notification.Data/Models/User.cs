﻿using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Notification.Data.Models
{
    public class User
    {
        public long Id { get; set; }

        public int LevelId { get; set; }

        public UserRole? Role { get; set; }

        public string Name { get; set; } = "";

        public UserLevel Level { get; set; } = new();

        public List<NotificationRecipient> NotificationRecipients { get; set; } = new();

        public List<UserNotification> UserNotifications { get; set; } = new();

        public UserDTO ToUserDTO()
        {
            return new() 
            { 
                Id = Id,
                LevelId = LevelId,
                Role = Role,
                Name = Name
            };
        }
    }
}
