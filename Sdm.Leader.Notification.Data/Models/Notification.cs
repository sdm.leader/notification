﻿using Sdm.Leader.Core.DTO;

namespace Sdm.Leader.Notification.Data.Models
{
    public class Notification
    {
        public long Id { get; set; }

        public string? Data { get; set; }

        public List<UserNotification> UserNotifications { get; set; } = new();

        public NotificationDTO ToNotificationDTO()
        {
            return new() 
            { 
                Id = Id,
                Data = Data
            };
        }

        public static Notification FromNotificationDTO(NotificationDTO item)
        {
            return new()
            {
                Id = item.Id,
                Data = item.Data
            };
        }
    }
}
