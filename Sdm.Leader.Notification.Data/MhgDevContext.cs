﻿using Microsoft.EntityFrameworkCore;
using Sdm.Leader.Notification.Data.Models;
using NotificationEntity = Sdm.Leader.Notification.Data.Models.Notification;

namespace Sdm.Leader.Notification.Data
{
    public sealed class MhgDevContext : DbContext
    {
        public MhgDevContext(DbContextOptions<MhgDevContext> options) : base(options) { }

        public DbSet<NotificationEntity> Notifications { get; set; }

        public DbSet<NotificationRecipient> NotificationRecipients { get; set; }

        public DbSet<User> Users { get; set; }

        public DbSet<UserLevel> UserLevels { get; set; }

        public DbSet<UserNotification> UserNotifications { get; set; }

        public DbSet<UserQueueNotification> UserQueueNotifications { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasPostgresEnum("user_role", new[] { "administrator", "distributor", "marketer", "sales" });

            modelBuilder.Entity<NotificationEntity>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("notifications_pkey");

                entity.ToTable("notifications");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.Data).HasColumnName("data");
            });

            modelBuilder.Entity<NotificationRecipient>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("notification_recipients_pkey");

                entity.ToTable("notification_recipients");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.UserRole).HasColumnName("user_role");
                entity.Property(e => e.UserId).HasColumnName("user_id");
                entity.Property(e => e.UserLevelId).HasColumnName("user_level_id");

                entity.HasOne(d => d.User).WithMany(p => p.NotificationRecipients)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("notification_recipients_user_fk");

                entity.HasOne(d => d.UserLevel).WithMany(p => p.NotificationRecipients)
                    .HasForeignKey(d => d.UserLevelId)
                    .HasConstraintName("notification_recipients_user_levels_fk");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("user_pkey");

                entity.ToTable("user");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.LevelId).HasColumnName("level_id");
                entity.Property(e => e.Role).HasColumnName("role");
                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.HasOne(d => d.Level).WithMany(p => p.Users)
                    .HasForeignKey(d => d.LevelId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_fk");
            });

            modelBuilder.Entity<UserLevel>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("user_levels_pkey");

                entity.ToTable("user_levels");

                entity.Property(e => e.Id)
                    .ValueGeneratedNever()
                    .HasColumnName("id");
                entity.Property(e => e.Description).HasColumnName("description");
            });

            modelBuilder.Entity<UserNotification>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("user_notification_pkey");

                entity.ToTable("user_notification");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.NotificationId).HasColumnName("notification_id");
                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Notification).WithMany(p => p.UserNotifications)
                    .HasForeignKey(d => d.NotificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_notification_notifications_fk");

                entity.HasOne(d => d.User).WithMany(p => p.UserNotifications)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_notification_user_fk");
            });

            modelBuilder.Entity<UserQueueNotification>(entity =>
            {
                entity.HasKey(e => e.Id).HasName("user_queue_notifications_pk");

                entity.ToTable("user_queue_notifications");

                entity.Property(e => e.Id).HasColumnName("id");
                entity.Property(e => e.NotificationId).HasColumnName("notification_id");
                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.HasOne(d => d.Notification).WithMany()
                    .HasForeignKey(d => d.NotificationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_queue_notifications_notifications_fk");

                entity.HasOne(d => d.User).WithMany()
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("user_queue_notifications_user_fk");
            });
        }
    }
}
