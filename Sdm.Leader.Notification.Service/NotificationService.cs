﻿using Microsoft.EntityFrameworkCore;
using Sdm.Leader.Core.DTO;
using Sdm.Leader.Core.Service;
using Sdm.Leader.Notification.Data.Models;
using Sdm.Leader.Notification.Data;
using NotificationEntity = Sdm.Leader.Notification.Data.Models.Notification;

namespace Sdm.Leader.Notification.Service
{
    public sealed class NotificationService : INotificationService
    {
        private readonly IDbContextFactory<MhgDevContext> _contextFactory;
        private readonly INotificationCheckerService _notificationCheckerService;

        public NotificationService(
            IDbContextFactory<MhgDevContext> contextFactory,
            INotificationCheckerService notificationCheckerService)
        {
            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
            _notificationCheckerService = notificationCheckerService ?? throw new ArgumentNullException(nameof(notificationCheckerService));
        }

        public async Task<NotificationDTO?> GetAsync(long id, CancellationToken cancellationToken)
        {
            using var context = _contextFactory.CreateDbContext();

            return await context.Notifications
                .Where(notification => notification.Id == id)
                .AsNoTracking()
                .Select(notification => notification.ToNotificationDTO())
                .FirstOrDefaultAsync(cancellationToken);
        }

        public void Create(NotificationDTO notification, CancellationToken cancellationToken)
        {
            using var context = _contextFactory.CreateDbContext();
            using var transaction = context.Database.BeginTransaction();

            var notificationEntity = NotificationEntity.FromNotificationDTO(notification);

            context.Notifications.Add(notificationEntity);

            context.SaveChanges();

            AddToUserQueueNotifications(context, notificationEntity);

            context.SaveChanges();

            transaction.Commit();

            _notificationCheckerService.Check(cancellationToken);
        }

        public static void AddToUserQueueNotifications(MhgDevContext context, NotificationEntity notification)
        {
            var recipients = context.NotificationRecipients
                .AsNoTracking()
                .ToList();

            foreach (var recipient in recipients)
            {
                var users = new Dictionary<long, User>();

                if (recipient.UserRole.HasValue)
                {
                    AddUsers
                    (
                        users,
                        GetUsersByRole(context, recipient.UserRole.Value)
                    );
                }

                if (recipient.UserLevelId.HasValue)
                {
                    AddUsers
                    (
                        users,
                        GetUsersByLevel(context, recipient.UserLevelId.Value)
                    );
                }

                if (recipient.UserId.HasValue)
                {
                    AddUsers
                    (
                        users,
                        new() { GetUserById(context, recipient.UserId.Value) }
                    );
                }

                foreach (var user in users)
                {
                    context.UserQueueNotifications.Add
                    (
                        new UserQueueNotification
                        {
                            User = user.Value,
                            Notification = notification
                        }
                    );
                }
            }
        }

        public static List<User> GetUsersByRole(MhgDevContext context, UserRole role)
        {
            return context.Users.Where(user => user.Role == role).ToList();
        }

        public static List<User> GetUsersByLevel(MhgDevContext context, int levelId)
        {
            return context.Users.Where(user => user.LevelId == levelId).ToList();
        }

        public static User GetUserById(MhgDevContext context, long id)
        {
            return context.Users.Where(user => user.Id == id).First();
        }

        public static void AddUsers(Dictionary<long, User> users, List<User> list)
        {
            foreach (var item in list)
            {
                users[item.Id] = item;
            }
        }
    }
}
