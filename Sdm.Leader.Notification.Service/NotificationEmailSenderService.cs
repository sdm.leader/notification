﻿using Microsoft.Extensions.Logging;
using Sdm.Leader.Core.DTO;
using Sdm.Leader.Core.Service;

namespace Sdm.Leader.Notification.Service
{
    public sealed class NotificationEmailSenderService : INotificationSenderService
    {
        private readonly ILogger<NotificationEmailSenderService>? _logger;

        public NotificationEmailSenderService(ILogger<NotificationEmailSenderService>? logger)
        {
            _logger = logger;
        }

        public Task SendAsync(UserDTO user, NotificationDTO notification, CancellationToken cancellationToken)
        {
            _logger?.LogInformation("Notification sended, user={UserName}, notification={NotificationData}", user.Name, notification.Data);

            return Task.CompletedTask;
        }
    }
}
