﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Sdm.Leader.Core.Service;
using Sdm.Leader.Notification.Data;
using Sdm.Leader.Notification.Data.Models;

namespace Sdm.Leader.Notification.Service
{
    public sealed class NotificationCheckerService : INotificationCheckerService
    {
        private readonly object _lock;

        private readonly IDbContextFactory<MhgDevContext> _contextFactory;
        private readonly INotificationSenderService _notificationSenderService;
        private readonly ILogger<NotificationCheckerService>? _logger;

        private Task? _task;
        private bool _stopRequested;

        public NotificationCheckerService(
            IDbContextFactory<MhgDevContext> contextFactory,
            INotificationSenderService notificationSenderService,
            ILogger<NotificationCheckerService>? logger)
        {
            _lock = new object();

            _contextFactory = contextFactory ?? throw new ArgumentNullException(nameof(contextFactory));
            _notificationSenderService = notificationSenderService ?? throw new ArgumentNullException(nameof(notificationSenderService));
            _logger = logger;

            _stopRequested = false;
        }

        public void Check(CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested || _stopRequested) return;

            lock (_lock)
            {
                _task?.Wait(cancellationToken);

                _task = CheckAsync(cancellationToken);
            }

        }

        public void Stop(CancellationToken cancellationToken)
        {
            lock (_lock)
            {
                _stopRequested = true;

                _task?.Wait(cancellationToken);
            }
        }

        public async Task CheckAsync(CancellationToken cancellationToken)
        {
            using var context = _contextFactory.CreateDbContext();            

            while(true)
            {
                long notificationId = 0L;
                long userId = 0L;

                try
                {
                    using var transaction = context.Database.BeginTransaction();

                    var item = await context.UserQueueNotifications
                        .Include(item => item.User)
                        .Include(item => item.Notification)
                        .OrderBy(item => item.Id)
                        .FirstOrDefaultAsync(cancellationToken);

                    if (item == null) return;

                    notificationId = item.NotificationId;
                    userId = item.UserId;

                    context.UserQueueNotifications.Remove(item);

                    var userNotification = new UserNotification
                    {
                        Notification = item.Notification,
                        User = item.User
                    };

                    context.UserNotifications.Add(userNotification);

                    context.SaveChanges();

                    await _notificationSenderService.SendAsync(item.User.ToUserDTO(), item.Notification.ToNotificationDTO(), cancellationToken); 
                    
                    await transaction.CommitAsync(cancellationToken);
                }
                catch (Exception exception)
                {
                    _logger?.LogWarning
                    (
                        exception,
                        "Error on send message to userId={UserId}, notificationId={NotificationId}",
                        userId,
                        notificationId
                    );
                }
            }
        }
    }
}