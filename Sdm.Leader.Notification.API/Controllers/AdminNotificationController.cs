﻿using Microsoft.AspNetCore.Mvc;
using Sdm.Leader.Core.DTO;
using Sdm.Leader.Core.Service;

namespace Sdm.Leader.Notification.API.Controllers
{
    [Route("/admin/notification")]
    public sealed class AdminNotificationController : Controller
    {
        private readonly INotificationService _notificationService;

        public AdminNotificationController(INotificationService notificationService)
        {
            _notificationService = notificationService ?? throw new ArgumentNullException(nameof(notificationService));
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(long id, CancellationToken cancellationToken)
        {
            return Ok
            (
                await _notificationService.GetAsync(id, cancellationToken)
            );
        }

        [HttpPost("create")]
        public IActionResult Create([FromBody] NotificationDTO notification, CancellationToken cancellationToken)
        {
            _notificationService.Create
            (
                notification,
                cancellationToken
            );

            return Ok();
        }
    }
}
