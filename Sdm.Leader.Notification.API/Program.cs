using Microsoft.EntityFrameworkCore;
using Npgsql;
using Sdm.Leader.Core.DTO;
using Sdm.Leader.Core.Service;
using Sdm.Leader.Notification.API.Tasks;
using Sdm.Leader.Notification.Data;
using Sdm.Leader.Notification.Service;

namespace Sdm.Leader.Notification.API
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            builder.Services.AddDbContextFactory<MhgDevContext>(options =>
            {
                var dataSourceBuilder = new NpgsqlDataSourceBuilder
                (
                    builder.Configuration.GetValue<string>("ConnectionStrings:MhgDev")
                );

                dataSourceBuilder.MapEnum<UserRole>();

                var dataSource = dataSourceBuilder.Build();

                options.UseNpgsql(dataSource);
            });

            builder.Services.AddSingleton<NotificationEmailSenderService>();
            builder.Services.AddSingleton<INotificationSenderService>(options => options.GetRequiredService<NotificationEmailSenderService>());

            builder.Services.AddSingleton<NotificationCheckerService>();
            builder.Services.AddSingleton<INotificationCheckerService>(options => options.GetRequiredService<NotificationCheckerService>());

            builder.Services.AddSingleton<NotificationService>();
            builder.Services.AddSingleton<INotificationService>(options => options.GetRequiredService<NotificationService>());

            builder.Services.AddHostedService<NotificationSenderTask>();

            builder.Services.AddControllers();

            var app = builder.Build();

            app.MapControllers();

            app.Run();
        }
    }
}